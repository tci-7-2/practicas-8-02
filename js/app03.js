function hacerPeticion() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    // Validar la respuesta
    http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Aquí se dibuja la página
            const detallesUsuariosBody = document.getElementById("detallesUsuariosBody");
            const usuarios = JSON.parse(this.responseText);

            // Limpiar el contenido existente
            detallesUsuariosBody.innerHTML = "";

            // Construir filas de la tabla con la información de los usuarios
            usuarios.forEach(usuario => {
                const fila = document.createElement("tr");
                const columnas = [
                    usuario.id,
                    usuario.name,
                    usuario.username,
                    usuario.email,
                    `${usuario.address.street}, ${usuario.address.suite}, ${usuario.address.city}, ${usuario.address.zipcode}, ${usuario.address.geo.lat}, ${usuario.address.geo.lng}`,
                    usuario.phone,
                    usuario.website,
                    `${usuario.company.name} "${usuario.company.catchPhrase}, ${usuario.company.bs}"`
                ];

                columnas.forEach(columna => {
                    const celda = document.createElement("td");
                    celda.textContent = columna;
                    fila.appendChild(celda);
                });

                detallesUsuariosBody.appendChild(fila);
            });
        }
    };

    http.open('GET', url, true);
    http.send();
}

function limpiarDetalles() {
    let detallesUsuariosBody = document.getElementById("detallesUsuariosBody");
    detallesUsuariosBody.innerHTML = "";  // Solo limpiar las filas del cuerpo de la tabla
}

document.getElementById("btnCargar").addEventListener("click", hacerPeticion);
document.getElementById("btnLimpiar").addEventListener("click", limpiarDetalles);
