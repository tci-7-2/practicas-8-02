function hacerPeticion() {
    const http = new XMLHttpRequest();
    const urlBase = "https://jsonplaceholder.typicode.com/albums";
    const idConsulta = document.getElementById("consulta").value.trim();

    if (!idConsulta || isNaN(idConsulta)) {
        alert("Por favor, ingresa un ID válido.");
        return;
    }

    const url = `${urlBase}/${idConsulta}`;

    http.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            const datos = JSON.parse(this.responseText);

            if (Object.keys(datos).length === 0) {
                alert("No se encontraron datos para el ID proporcionado.");
                limpiarResultado();
                return;
            }

            mostrarResultado(datos);
        }
    };

    http.open('GET', url, true);
    http.send();
}

function limpiarResultado() {
    document.getElementById("resultado").innerHTML = "";
}

function mostrarResultado(datos) {
    const resultadoContainer = document.getElementById("resultado");

    resultadoContainer.innerHTML = `<div class="resultado-label">ID Usuario:</div>
                                    <div>${datos.userId}</div>
                                    <div class="resultado-label">ID:</div>
                                    <div>${datos.id}</div>
                                    <div class="resultado-label">Título:</div>
                                    <div>${datos.title}</div>`;
}

document.getElementById("btnConsulta").addEventListener("click", function() {
    hacerPeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", function() {
    limpiarResultado();
    document.getElementById("consulta").value = "";
});
