function mostrarUsuario() {
    const userIdInput = document.getElementById("userId");
    const userId = userIdInput.value.trim();

    if (userId === "") {
        alert("Ingrese un ID de usuario válido.");
        return;
    }

    const http = new XMLHttpRequest();
    const url = `https://jsonplaceholder.typicode.com/users/${userId}`;

    // Validar la respuesta
    http.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                // Aquí se dibuja la información del usuario
                const infoUsuarioContainer = document.getElementById("infoUsuario");
                const usuario = JSON.parse(this.responseText);

                // Limpiar el contenido existente
                infoUsuarioContainer.innerHTML = "";

                // Mostrar la información del usuario
                Object.entries(usuario).forEach(([clave, valor]) => {
                    const parrafo = document.createElement("p");

                    if (typeof valor === 'object' && clave !== 'geo') {
                        parrafo.textContent = `${clave}:`;

                        // Crear una lista para propiedades anidadas
                        const listaAnidada = document.createElement("ul");

                        Object.entries(valor).forEach(([subclave, subvalor]) => {
                            const itemLista = document.createElement("li");

                            // Manejar el caso especial de "geo" dentro de "address"
                            if (subclave === 'geo' && clave === 'address') {
                                Object.entries(subvalor).forEach(([geoClave, geoValor]) => {
                                    const geoItemLista = document.createElement("li");
                                    geoItemLista.textContent = `${geoClave}: ${geoValor}`;
                                    listaAnidada.appendChild(geoItemLista);
                                });
                            } else {
                                itemLista.textContent = `${subclave}: ${subvalor}`;
                                listaAnidada.appendChild(itemLista);
                            }
                        });

                        // Agregar la lista al párrafo
                        parrafo.appendChild(listaAnidada);
                    } else {
                        parrafo.textContent = `${clave}: ${valor}`;
                    }

                    infoUsuarioContainer.appendChild(parrafo);
                });
            } else {
                alert("No se encontró ningún usuario con ese ID.");
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

function limpiarInfoUsuario() {
    const infoUsuarioContainer = document.getElementById("infoUsuario");
    infoUsuarioContainer.innerHTML = "";
    document.getElementById("userId").value = "";
}

document.getElementById("btnMostrar").addEventListener("click", mostrarUsuario);
document.getElementById("btnLimpiar").addEventListener("click", limpiarInfoUsuario);
